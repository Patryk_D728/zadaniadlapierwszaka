class GameResult:
    def __init__(self, rounds):
        self._correct_answers = 0
        self._wrong_answers = 0
        self._percentage_of_correct_answers = 0
        self._rounds_to_end = rounds
        self._last_correct_answer = None

    def __str__(self):
        percentage = "{:.0%}".format(self._percentage_of_correct_answers)
        return f'''
Poprawnie \t| Błędnie \t| Procent poprawnych \t| Do końca
{self._correct_answers} \t\t\t| {self._wrong_answers} \t\t| {percentage} \t\t\t\t\t| {self._rounds_to_end} 
        '''

    def _update(self):
        self._percentage_of_correct_answers = self._correct_answers / (self._correct_answers + self._wrong_answers)
        self._rounds_to_end -= 1

    def add_correct_answer(self):
        self._correct_answers += 1
        self._last_correct_answer = True
        self._update()

    def add_wrong_answer(self):
        self._last_correct_answer = False
        self._wrong_answers += 1
        self._update()
