import random
import math

class TaskGenerator:
    def __init__(self, operator, result_limit):
        self._operator = operator
        self._result_limit = result_limit
        self.add_task_generator = {
            '+': self._addition_task
        }
        self.sub_task_generator = {
            '-': self._subtract_task
        }
        self.mul_task_generator = {
            '*': self._multiple_task
        }
        self.div_task_generator = {
            '/': self._divide_task
        }


    def _addition_task(self):
        first_number = random.randint(0, self._result_limit)
        second_number = random.randint(0, self._result_limit - first_number)
        result = first_number + second_number
        return first_number, second_number, result
    def _subtract_task(self):
        first_number = random.randint(0, self._result_limit)
        second_number = random.randint(0, self._result_limit + first_number)
        result = first_number - second_number
        return first_number, second_number, result

    def _multiple_task(self):
        first_number = math.floor(random.uniform(0, self._result_limit))
        if first_number>0:
            second_number = math.floor(random.uniform(0, self._result_limit/first_number))
        else:
            second_number = 0
        result = first_number * second_number
        return first_number, second_number, result

    def _divide_task(self):
        first_number = random.randint(1, self._result_limit)
        if first_number%2==0:
            second_number = random.randint(1, first_number/2)
        else:
            second_number = random.randint(1, (first_number+1)/2)

        result = first_number/second_number
        return first_number, second_number, result

    def get_add_task(self):
        return self.add_task_generator[self._operator]()

    def get_mul_task(self):
        return self.mul_task_generator[self._operator]()

    def get_sub_task(self):
        return self.sub_task_generator[self._operator]()

    def get_div_task(self):
        return self.div_task_generator[self._operator]()