from GameResult import GameResult
from TaskGenerator import TaskGenerator


class Game:
    def __init__(self):
        self._operator = None
        self._result_limit = None
        self._rounds_number = None

    def _get_input(self):
        self._operator = input('Rodzaj działania: ')
        while True:
            try:
                self._result_limit = int(input('Ogranicz wynik: '))
            except ValueError:
                print("Not an integer!")
                continue
            break

        while True:
            try:
                self._rounds_number = int(input('Liczba rund: '))
            except ValueError:
                print("Not an integer!")
                continue
            break

    def start(self):
        global get_task
        self._get_input()
        game_result = GameResult(self._rounds_number)
        task_generator = TaskGenerator(self._operator, self._result_limit)
        print('Start!')

        for i in range(self._rounds_number):
            if self._operator == "+":
                first, second, result = task_generator.get_add_task()
            if self._operator == "*":
                first, second, result = task_generator.get_mul_task()
            if self._operator == "-":
                first, second, result = task_generator.get_sub_task()
            if self._operator == "/":
                first, second, result = task_generator.get_div_task()
            user_result = int(input(f'{first} {self._operator} {second} = '))
            print()
            if user_result == result:
                game_result.add_correct_answer()
                print('Dobrze!')
            else:
                game_result.add_wrong_answer()
                print('Źle!')
                print(f'{first} {self._operator} {second} = {result}')
            print(game_result)
