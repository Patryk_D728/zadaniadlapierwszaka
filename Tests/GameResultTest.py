import unittest
from GameResult import GameResult


class GameResultTest(unittest.TestCase):

    def test_add_correct_answer(self):
        result = GameResult(1)
        result.add_correct_answer()
        self.assertEqual(1, result._correct_answers)

    def test_add_wrong_answer(self):
        result = GameResult(1)
        result.add_wrong_answer()
        self.assertEqual(1, result._wrong_answers)

    def test_count_rounds(self):
        rounds = 20
        result = GameResult(rounds)
        for i in range(rounds, 0, -1):
            self.assertEqual(i, result._rounds_to_end)
            result.add_wrong_answer()


if __name__ == '__main__':
    unittest.main()
