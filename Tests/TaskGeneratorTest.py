import unittest
from TaskGenerator import TaskGenerator


class TaskGeneratorTest(unittest.TestCase):
    def test_adding(self):
        for i in range(1000):
            generator = TaskGenerator('+', 1000)
            a, b, c = generator.get_add_task()
            self.assertEqual(a + b, c)
    def test_multiple(self):
        for i in range(1000):
            generator = TaskGenerator('*', 1000)
            a, b, c = generator.get_mul_task()
            self.assertEqual(a * b, c)
    def test_subtract(self):
        for i in range(1000):
            generator = TaskGenerator('-', 1000)
            a, b, c = generator.get_sub_task()
            self.assertEqual(a - b, c)
    def test_divide(self):
        for i in range(1000):
            generator = TaskGenerator('/', 1000)
            a, b, c = generator.get_div_task()
            self.assertEqual(a / b, c)

    def test_add_limit(self):
        for i in range(1000):
            generator = TaskGenerator('+', i)
            a, b, c = generator.get_add_task()
            self.assertTrue(c <= i)
    def test_mul_limit(self):
        for i in range(1000):
            generator = TaskGenerator('*', i)
            a, b, c = generator.get_mul_task()
            self.assertTrue(c <= i)

    def test_sub_limit(self):
        for i in range(1000):
            generator = TaskGenerator('-', i)
            a, b, c = generator.get_sub_task()
            self.assertTrue(c <= i)
    def test_div_limit(self):
        for i in range(1,1000):
            generator = TaskGenerator('/', i)
            a, b, c = generator.get_div_task()
            self.assertTrue(c <= i)


if __name__ == '__main__':
    unittest.main()
